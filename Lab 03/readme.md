#### Module 3

For module three, you'll add the final feature to your rating widget: the ability to send the 
rating to a service.

The service has already been created. The service will act as a simple echo service, taking the 
rating you submit inside a JSON object, and then sending back a JSON containing a message. The structure of the expected JSON object, and the JSON object the service will return, are documented below.

To complete the lab, follow these steps:

1. Update the HTML you used to complete lab two.

- Add a __button__. You will use the new button to submit the rating to the Rating service. 
You can use the HTML below:  
´´´
<button type="button" id="save-rating">Save rating</button>
´´´
- Add a __div__ element. You will use the new div element to output the message. 
You can use the HTML below:  
´´´
<div id="output"></div>
´´´

2. Add a click event handler for the button you created. Add the code necessary to call the Rating service. 
The information about the rating service, including the URL, the verb, and the JSON objects, is below.

3. When the server returns the object, display the __message__ inside of the div you created earlier.

__URL:__ [http://jquery-edx.azurewebsites.net/api/Rating](http://jquery-edx.azurewebsites.net/api/Rating)

__VERB:__ POST

__Parameter (object to upload):__ The server expects a JSON object with two properties, value, which 
contains the current rating, and maxValue, which contains the maximum allowed value. 
(Example: ´{ value: 4, maxValue: 5}´)

__Return (object returned by server):__ The server will return a JSON object with one property, message 
which will contain the message returned by the server. (Example: ´{message: 'You chose 4 out of 5'}´)


![ajax](ajax.png)


#### Module 2

For module two, you will enhance the interface for your review widget. The updated interface must meet the following goals

- Provide a configurable number of circles for the user to click on. This can be accomplished by adding a custom attribute 
to the outer __div__ element that contains the circles.
- Allow the number of circles to be updated by a textbox. This is to simulate an administration widget; we will __not__ be 
adding any form of security to the widget.
- The existing functionality of hovering and selection must still work.

The starting ratings widget will have 5 circles.

![fivecircles](fivecircles.png)

When a user enters a new value into the textbox, the number of circles must contain the number provided.

![tencircles](tencircles.png)

__Tip__: Delegate, which we talked about previously, does not support the __hover__ event.